package experiments_test

import (
	"path/filepath"
	"testing"

	"git.sr.ht/~ruivieira/go-experiments"
)

func TestGetAllFilesType(t *testing.T) {
	t.Parallel()

	goFiles := experiments.GetAllFilesType("./", ".go")

	for _, goFile := range *goFiles {
		if filepath.Ext(goFile) != ".go" {
			t.Error("Found files with different extension")
		}
	}
}
