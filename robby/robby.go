package main

import (
	"fmt"

	"git.sr.ht/~ruivieira/go-experiments/tasks"

	"git.sr.ht/~ruivieira/go-experiments"
)

func main() {
	lstasks := tasks.GetLogSeqAllTasks("/Users/rui/notes/logseq/pages")

	for _, lst := range *lstasks {
		fmt.Println(lst)
	}

	secret := experiments.LoadSecret("todoist")

	fmt.Println(secret.Token)

	todoist := tasks.NewTodoist(secret.Token)
	items := todoist.GetTodoistDoneTasks()
	for _, item := range *items {
		fmt.Printf("%+v\n", item)
	}
}
