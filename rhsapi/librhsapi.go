// Package rhsapi provides a client to the Red Hat Security API
package rhsapi

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"
)

// CVE stores data about the CVEs
type CVE struct {
	CVE                 string
	Severity            string
	PublicDate          string `json:"public_date"`
	Advisories          []string
	Bugzilla            string
	BugzillaDescription string  `json:"bugzilla_description"`
	CvssScore           float32 `json:"cvss_score"`
	CvssScoringVector   string  `json:"cvss_scoring_vector"`
	CWE                 string
	AffectedPackages    []string `json:"affected_packages"`
	ResourceURL         string   `json:"resource_url"`
	Cvss3ScoringVector  string   `json:"cvss3_scoring_vector"`
	Cvss3Score          string   `json:"cvss3_score"`
}

// CVEOptions contains the query options for the RHSAPI endpoint
type CVEOptions struct {
	Before         *time.Time
	After          *time.Time
	Ids            *[]string
	Bug            *int
	Advisory       *string
	Severity       *string
	Package        *string
	Product        *string
	CWE            *string
	CvssScrore     *float32
	Page           *int
	PerPage        *int
	CreatedDaysAgo *int
}

var myClient = &http.Client{Timeout: 10 * time.Second}

func getJSON(url string, target interface{}) error {
	r, err := myClient.Get(url)
	if err != nil {
		return err
	}
	defer r.Body.Close()

	return json.NewDecoder(r.Body).Decode(target)
}

func main() {
	var cves []CVE
	getJSON("https://access.redhat.com/hydra/rest/securitydata/cve.json", &cves)
	fmt.Println(cves)
}
