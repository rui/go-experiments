package tasks_test

import (
	"fmt"
	"testing"

	"git.sr.ht/~ruivieira/go-experiments/tasks"

	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

func TestJoplinConnection(t *testing.T) {
	db, _ := gorm.Open(sqlite.Open(tasks.JoplinDBLocation()), &gorm.Config{})
	var users []tasks.JoplinNote
	db.Limit(10).Order("created_time desc").Find(&users)
	for _, user := range users {
		fmt.Println(user.CreatedTime)
	}
}
