package tasks

import "time"

// Task stores information about a LogSeq task.
type Task struct {
	Status  string
	Content string
	Due     time.Time
}
