// Package Joplin Utilities to parse Joplin's database
package tasks

import (
	"log"
	"os"
	"path/filepath"
)

type JoplinNote struct {
	Id          string `gorm:"primaryKey"`
	ParentId    string
	Title       string
	Body        string
	CreatedTime int64
	UpdatedTime int64
}

func (JoplinNote) TableName() string {
	return "notes"
}

func JoplinDBLocation() string {
	dirname, err := os.UserHomeDir()
	if err != nil {
		log.Fatal(err)
	}
	path := filepath.Join(dirname, ".config", "joplin-desktop", "database.sqlite")
	return path
}
