package tasks

import (
	"context"
	"fmt"
	"log"

	td "github.com/kobtea/go-todoist/todoist"
)

// Todoist holds the information of Todoist account
type Todoist struct {
	Client  *td.Client
	Context *context.Context
}

func toTask(item *td.Item) *Task {
	var status string
	if item.Checked {
		status = "TODO"
	} else {
		status = "DONE"
	}

	return &Task{
		Status:  status,
		Content: item.Content,
		Due:     item.Due.Date.Time,
	}
}

// NewTodoist creates a new generic client
func NewTodoist(token string) *Todoist {
	cli, _ := td.NewClient("", token, "*", "", nil)
	ctx := context.Background()
	err := cli.FullSync(ctx, []td.Command{})
	if err != nil {
		log.Fatal(err)
	}
	return &Todoist{Client: cli, Context: &ctx}
}

// GetTodoistAllTasks returns all tasks, regardless of status
func (td *Todoist) GetTodoistAllTasks() *[]Task {
	items := td.Client.Item.GetAll()
	var result []Task
	for _, item := range items {
		task := toTask(&item) // Parse the Todoist item to a `Task`
		result = append(result, *task)
	}
	return &result
}

// GetTodoistDoneTasks returns all completed tasks
func (td *Todoist) GetTodoistDoneTasks() *[]Task {
	items := td.Client.Completed.Item.GetAll()
	var result []Task
	for _, item := range items {
		fmt.Printf("%+v\n", item)
		task := toTask(&item) // Parse the Todoist item to a `Task`
		result = append(result, *task)
	}
	return &result
}

// GetTodoistPendingTasks returns all completed tasks
func (td *Todoist) GetTodoistPendingTasks() *[]Task {
	items := td.Client.Item.Completed.Item.GetAll()
	var result []Task
	for _, item := range items {
		fmt.Printf("%+v\n", item)
		task := toTask(&item) // Parse the Todoist item to a `Task`
		result = append(result, *task)
	}
	return &result
}
