// Package tasks parses LogSeq task files
package tasks

import (
	"io/ioutil"
	"log"
	"strings"

	"git.sr.ht/~ruivieira/go-experiments"
)

// ParseLogSeqTask takes a string and returns the parsed task (or `nil` if none)
func ParseLogSeqTask(line *string) *Task {
	trimmedLine := strings.TrimLeft(*line, "\t -")
	if len(trimmedLine) > 4 && strings.HasPrefix(trimmedLine, "TODO") {
		return &Task{Status: "TODO", Content: (trimmedLine)[5:]}
	} else if len(trimmedLine) > 5 && strings.HasPrefix(trimmedLine, "LATER") {
		return &Task{Status: "LATER", Content: (trimmedLine)[6:]}
	} else if len(trimmedLine) > 4 && strings.HasPrefix(trimmedLine, "DONE") {
		return &Task{Status: "DONE", Content: (trimmedLine)[5:]}
	}
	return nil
}

// GetLogSeqAllTasks retrieves all org-mode tasks from root.
func GetLogSeqAllTasks(root string) *[]*Task {
	files := experiments.GetAllFilesType(root, ".md")

	var tasks []*Task

	for _, file := range *files {
		content, err := ioutil.ReadFile(file)
		if err != nil {
			log.Fatal(err)
		}

		lines := strings.Split(string(content), "\n")

		for _, line := range lines {
			task := ParseLogSeqTask(&line)

			if task != nil {
				tasks = append(tasks, task)
			}
		}
	}

	return &tasks
}
