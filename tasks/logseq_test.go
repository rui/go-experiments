package tasks_test

import (
	"testing"

	"git.sr.ht/~ruivieira/go-experiments/tasks"
)

func TestParseTODO(t *testing.T) {
	t.Parallel()

	taskStr := "- TODO This is a test"
	task := tasks.ParseLogSeqTask(&taskStr)
	if task == nil {
		t.Error("Task shouldn't be nil")
	}
	if task.Status != "TODO" {
		t.Error("Wrong status")
	}
	if task.Content != "This is a test" {
		t.Error("Wrong content")
	}

	taskStr = "			- TODO I am outdented"
	task = tasks.ParseLogSeqTask(&taskStr)
	if task == nil {
		t.Error("Task shouldn't be nil")
	}
	if task.Status != "TODO" {
		t.Error("Wrong status")
	}
	if task.Content != "I am outdented" {
		t.Error("Wrong content")
	}
}
