module git.sr.ht/~ruivieira/go-experiments

go 1.16

require (
	github.com/kobtea/go-todoist v0.2.2
	github.com/kr/pretty v0.1.0 // indirect
	github.com/mattn/go-isatty v0.0.4 // indirect
	golang.org/x/sys v0.0.0-20210608053332-aa57babbf139 // indirect
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
	gorm.io/driver/sqlite v1.2.6
	gorm.io/gorm v1.22.4
)
