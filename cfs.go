// Package experiments Filesystem common utilities
package experiments

import (
	ofs "io/fs"
	"log"
	"path/filepath"
)

// GetAllFilesType Gets all files recursively matching extension.
func GetAllFilesType(root string, extension string) *[]string {
	var files []string

	e := filepath.WalkDir(root, func(path string, d ofs.DirEntry, err error) error {
		if filepath.Ext(path) == extension {
			files = append(files, path)
		}

		return err
	})

	if e != nil {
		log.Fatal(e)
	}

	return &files
}
