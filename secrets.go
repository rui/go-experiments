// Package experiments Manage secrets in .config
package experiments

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
)

// Secret holds data about a loaded secret
type Secret struct {
	Credentials string `json:"credentials"`
	Token       string `json:"token"`
}

// LoadSecret loads a secret from a topic (`~/.config/<topic>/credentials.json`)
func LoadSecret(topic string) *Secret {
	dirname, err := os.UserHomeDir()
	if err != nil {
		log.Fatal(err)
	}
	path := filepath.Join(dirname, ".config", topic, "credentials.json")
	jsonFile, err := os.Open(path)
	// if we os.Open returns an error then handle it
	if err != nil {
		fmt.Println(err)
	}
	// read our opened jsonFile as a byte array.
	byteValue, _ := ioutil.ReadAll(jsonFile)
	// we initialize our Users array
	var secret Secret

	// we unmarshal our byteArray which contains our
	// jsonFile's content into 'users' which we defined above
	err = json.Unmarshal(byteValue, &secret)
	if err != nil {
		return nil
	}

	// defer the closing of our jsonFile so that we can parse it later on
	defer jsonFile.Close()
	return &secret
}
