// Package readme auto-generates this README.md
package readme

import (
	"bytes"
	_ "embed" // to embed the template
	"io/ioutil"
	"log"
	"os"
	p "path"
	"path/filepath"
	"sort"
	"strings"
	"text/template"
)

// Info stores information about an indexable project
type Info struct {
	Path     string
	Package  string
	Basename string
	Info     string
}

//go:embed README.md.tmpl
var templateStr string

var logger = log.New(os.Stdout, "INFO: ", log.Ldate|log.Ltime|log.Lshortfile)

// ParseInfo reads the repository recursively and creates the README.md
func ParseInfo(rootPath string, templatePath string, outputPath string) {
	var infos []Info

	logger.Println("Starting with project root: " + rootPath)

	err := filepath.Walk(rootPath,
		func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}
			if filepath.Ext(path) == ".go" {
				content, err := ioutil.ReadFile(path)
				if err != nil {
					logger.Fatal(err)
				}
				text := string(content)
				// get first line
				firstLine := strings.Split(text, "\n")[0]
				logger.Println("First line " + firstLine)
				if firstLine[0:10] == "// Package" {
					logger.Println("Found file with INFO on " + path)
					tokens := strings.Split(firstLine[10:], " ")
					var info Info
					info.Path, _ = filepath.Rel(rootPath, path)
					info.Package = tokens[1]
					info.Info = strings.Join(tokens[2:], " ")
					info.Basename = p.Base(path)
					infos = append(infos, info)
				}
			}

			// Convert []byte to string and print to screen
			return nil
		})
	if err != nil {
		log.Println(err)
	}

	sort.Slice(infos, func(i, j int) bool {
		iStr := infos[i].Info + infos[i].Package + infos[i].Basename
		jStr := infos[j].Info + infos[j].Package + infos[j].Basename
		return len(iStr) < len(jStr)
	})
	log.Println(infos)

	t, err := template.New("README").Parse(templateStr)
	if err != nil {
		log.Fatalln(err)
	}

	buf := &bytes.Buffer{}
	err = t.Execute(buf, infos)

	if err != nil {
		log.Println(err)
	}

	result := buf.String()
	f, err := os.Create(outputPath + "/README.md")
	if err != nil {
		log.Fatalln(err)
	}

	_, err = f.WriteString(result)
	if err != nil {
		log.Fatalln(err)
	}

	log.Println(result)
}
