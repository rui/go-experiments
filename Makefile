.PHONY: all test lint vet fmt build

SOURCES := $(shell git ls-files '*.go')

all: test lint vet fmt build
test:
	go test -v ./...
lint:
	`go env GOPATH`/bin/golint -set_exit_status ./...
vet:
	go vet ./...
fmt:
	gofmt -s -l . && [ -z "$(gofmt -s -l .)" ]
build:
	go build