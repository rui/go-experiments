[![builds.sr.ht status](https://builds.sr.ht/~ruivieira/go-experiments/commits/.build.yml.svg)](https://builds.sr.ht/~ruivieira/go-experiments/commits/.build.yml?)
# go-experiments
![](docs/gopher-launch.jpg)

* [logseq.go](tasks/logseq.go), `tasks`, parses LogSeq task files
* [cfs.go](cfs.go), `experiments`, Filesystem common utilities
* [readme.go](readme/readme.go), `readme`, auto-generates this README.md
* [secrets.go](secrets.go), `experiments`, Manage secrets in .config
* [joplin.go](tasks/joplin.go), `Joplin`, Utilities to parse Joplin's database
* [librhsapi.go](rhsapi/librhsapi.go), `rhsapi`, provides a client to the Red Hat Security API
